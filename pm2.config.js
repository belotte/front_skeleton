const NODE_ENV = process.env.NODE_ENV
const base_dir = process.env.PWD

module.exports = {
	apps: [{
		script: 'index.js',
		name: 'numbers_back',
		cwd: `${base_dir}/app/back`,
		instances: 1,
		exec_mode: 'fork',
		merge_logs: true,
		env: {
			PORT: 4011,
			TZ: 'Europe/Paris',
			NODE_ENV
		},
		error_file: `${base_dir}/logs/numbers_back.error`,
		out_file: `${base_dir}/logs/numbers_back.log`
	}, {
		script: 'npm run start',
		name: 'numbers_front',
		cwd: `${base_dir}/app/front`,
		instances: 1,
		exec_mode: 'fork',
		merge_logs: true,
		exec_interpreter: 'none',
		env: {
			PORT: 4022,
			TZ: 'Europe/Paris',
			NODE_ENV
		},
		error_file: `${base_dir}/logs/numbers_front.error`,
		out_file: `${base_dir}/logs/numbers_front.log`
	}]
}
