import React from 'react'
import {
	BrowserRouter as Router,
	Route
} from 'react-router-dom'
import './App.css'

function App (props) {
	return (
		<Router>
			<Route path = '/:param?' children = {({ match: { params }, history }) => (
				<div
					identifier = {params.param}
					history = {history}
					/>
				)} />
		</Router>
	)
}

export default App
