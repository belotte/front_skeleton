/**
 * Chaque instance représente le résultat d'une requète lancée par un cron UNIX.
 *
 * L'utilisation des crons UNIX a plusieurs avantages par rapport aux systemes de cron fournis par certains modules:
 * - support des serveur en mode cluster
 * - possibilité de lister les crons actif sur le serveur directement
 * - possibilité de relancer un cron en lancant simplement une requete GET
 *
 * Pour configurer des crons sur le serveur: `crontab -e`.
 *
 * Pour le format du cron, se référer a la page Wikipedia.
 *
 * La requete a lancer doit etre de la forme:
 *
 * `curl -X GET <host>/api/v2/crons/run/<nom_du_cron>`
 *
 * @property {boolean} success        Vrai si le cron s'est exécuté sans erreur.
 * @property {string}  status         Status.
 * @property {string}  action         Nom de l'action du cron.
 * @property {string}  error          Descriptionde l'erreur rencontré.
 * @property {number}  execution_time Temps de l'exécution du cron, en secondes.
 */
class Cron extends Model {
	static init (sequelize, data_type) {
		return super.init ({
			success:				data_type.BOOLEAN,
			status:				data_type.ENUM ([
				'not found',
				'pending',
				'done',
				'failed',
				'retried',
				'closed'
			]),
			action:				data_type.TEXT,
			error:				data_type.TEXT,
			execution_time:	data_type.DECIMAL
		}, {
			modelName: 'cron',
			sequelize
		})
	}

	static associate (models) {
		this.belongsTo (Cron, {
			as: 'next_try',
			foreignKey: 'next_try_id'
		})
	}
}

/**
* Passe le status du cron en `closed`.
* @memberof Cron
* @method
*/
Cron.prototype.close = async function ({ } = { }) {
	console.function_called (`[${this.id}]`)

	if (!['pending'].includes (this.status)) {
		throw 'Only pending crons can be closed.'
	}

	await this.update ({
		status: 'closed'
	})
}

/**
* Relance le cron.
*
* Un nouveau cron sera créé, et associé a celui relancé, via `next_try_id`.
* @memberof Cron
* @method
*/
Cron.prototype.retry = async function ({ } = { }) {
	console.function_called (`[${this.id}]`)

	if (['retried'].includes (this.status)) {
		throw 'This cron has already been retried.'
	}
	await requests.get ({
		url: `http://localhost:${process.env.PORT}/api/v2/crons/run/${this.action}/${this.id}`
	})
}

module.exports = Cron
