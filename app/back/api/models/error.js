/**
 * Une grande partie des routes est englobée dans une fonction `wrap` qui catch les erreurs généré pendant la requete.
 *
 * Pour chaque erreur, un **error** est généré, sauvegardant toutes les informations relatives a la requete.<br>
 * Il est alors possible de re-tenter la requete, avec l'action `retry`.
 *
 * @property {string} route      Route sur laquelle l'erreur est survenue.
 * @property {string} method     Méthode.
 * @property {Object} errors     Érreurs.
 * @property {Object} body       Corps envoyé.
 * @property {Object} headers    Headers.
 * @property {Object} parameters Paramètres query.
 * @property {string} status     Status.
 * @property {number} retries    Nombre de fois que la requête a été re-tenté.
 */
class Error extends Model {
	static init (sequelize, data_type) {
		return super.init ({
			route:				data_type.TEXT,
			method:				data_type.ENUM ([
				'GET',
				'POST',
				'PUT',
				'DELETE',
				'PATCH'
			]),
			errors: {
				type:				data_type.JSON,
				defaultValue:	[ ]
			},
			body: {
				type:				data_type.JSON,
				defaultValue:	{ }
			},
			headers: {
				type:				data_type.JSON,
				defaultValue:	{ }
			},
			parameters: {
				type:				data_type.JSON,
				defaultValue:	{
					query: { },
					params: { }
				}
			},
			status: {
				type:			data_type.ENUM ([
					'error',
					'pending',
					'done'
				]),
				defaultValue:	'error'
			},
			retries: {
				type:			data_type.INTEGER,
				defaultValue:	0
			}
		}, {
			modelName: 'error',
			sequelize,
			hooks: { }
		})
	}

	static associate (models) { }
}

/**
* Relance la requête qui a généré l'erreur.
* @memberof Error
* @method
*/
Error.prototype.retry = async function ({ } = { }) {
	console.function_called (`[${this.id || '_'}]`)

	if (!['error'].includes (this.status)) {
		throw 'This action can be runned on `error` Errors only'
	}

	await this.update ({
		status: 'pending',
		retries: this.retries + 1
	})

	let { body, error, status_code } = await requests[this.method.toLowerCase ()] ({
		url: `http://localhost:${process.env.PORT}${this.route}${this.route.includes ('?') && '&' || '?'}ERROR_ID=${this.id}`,
		body: this.body
	})

	await this.update ({
		status: (error || status_code === 500 || body.error) && 'error' || 'done'
	})
}

module.exports = Error
