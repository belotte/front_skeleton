const md5 = require ('md5')
const fs_extra = require ('fs-extra')

/**
 * Chaque instance de **file** représente un fichier physique stocké sur le serveur.
 * Les fichiers peuvent soit etre générés automatiquement, soit uploadés via **file**.`upload`.
 *
 * Les fichiers sont accessibles via <host>/api/v2/files/:token.
 * Le token est au format suivant: <string random de 32 caractères alphanumériques>.<hash md5 du fichier>.
 * Il est possible d'augmenter la sécurité en utilisant plus de caractères random, mais il existe déja 2E57 possibilités sur 32 caractères ;]
 *
 * @property {string}  type                Type.
 * @property {string}  label               Étiquette.
 * @property {string}  path                Chemin du fichier sur le serveur.
 * @property {string}  filename            Nom.
 * @property {string}  token               Token.
 * @property {string}  owner               Nom du propriétaire, ou du créateur.
 * @property {boolean} enabled             Indique si le téléchargement est activé.
 * @property {number}  number_of_downloads Nombre de téléchargements.
 */
class File extends Model {
	static init (sequelize, data_type) {
		return super.init ({
			type: {
				type:					data_type.TEXT,
				defaultValue:		'file'
			},
			label:					data_type.TEXT,
			path:						data_type.TEXT,
			filename:				data_type.TEXT,
			token:					data_type.TEXT,
			hash:						data_type.TEXT,
			owner:					data_type.TEXT,
			enabled: {
				type:					data_type.BOOLEAN,
				defaultValue:		true
			},
			number_of_downloads: {
				type:					data_type.INTEGER,
				defaultValue: 0
				}
		}, {
			modelName: 'file',
			sequelize,
			hooks: {
				beforeCreate: async function (file) {
					file.path = file.path.replace (process.env.PWD, '').replace (/^\//, '')

					if (!file.filename) {
						file.filename = file.path.split ('/').last_element
					}
					if (/\/[a-zA-Z0-9_-]*\.[a-zA-Z0-9_-]*$/.test (file.path)) {
						file.path = file.path.replace (/\/[^\/]*$/, '')
					}
					if (!file.token) {
						let buff = fs.readFileSync (`${process.env.PWD}/${file.path}/${file.filename}`)
						file.token = `${rand (32).toLowerCase ()}.${md5 (buff)}`
					}
					file.hash = (file.token || '.').split ('.')[1]
				}
			}
		})
	}

	get link () {
		return `${hosts.local}/api/v2/files/${this.token}`
	}

	static associate (models) { }
}

/**
 * Calcule le token, créé le fichier physique sur le serveur, et créé l'instance correspondante.
 * `data` et `base64` sont concurrents. Si les 2 sont défini, `base64` sera utilisé.
 *
 * Si un fichier est trouvé avec le meme hash, alors le fichier n'est pas re-créé.
 *
 * TODO: ne pas permettre d'overwrite des fichiers serveurs.
 *
 * @memberof File
 * @method
 * @static
 * @param   {string} data        Texte brut a écrire dans le fichier.
 * @param   {string} base64      Données en base64.
 * @param   {string} [owner]     Nom du créateur.
 * @param   {string} path        Chemin dans lequel stocker le fichier, a partir de la racine du serveur.
 * @param   {stirng} filename    Nom du fichier, avec l'extension.
 * @param   {string} [type=file] Type.
 * @param   {string} [label]     Étiquette.
 * @returns {File}
 */
File.generate = async function ({ data, base64, owner, path, filename, type, label } = { }) {
	console.function_called (`[${path || '_'}][${owner || '_'}]`)

	path = path.replace (process.env.PWD, '').replace (/^\//, '')

	if (!filename) {
		filename = `${path.split ('/').last_element}`
	}
	if (/\/[a-zA-Z0-9_-]*\.[a-zA-Z0-9_-]*$/.test (path)) {
		path = path.replace (/\/[^\/]*$/, '')
	}
	let token = null

	if (base64) {
		data = base64.cleaned_base64
		token = `${rand (32).toLowerCase ()}.${data.base64_md5}`
	}
	else {
		token = `${rand (32).toLowerCase ()}.${md5 (data.toString ('base64'))}`
	}

	let existing_file = await models.file.findOne ({
		where: {
			hash: token.split ('.')[1]
		}
	})

	if (existing_file) {
		return existing_file
	}

	await fs_extra.ensureDir (`${process.env.PWD}/${path}`)

	fs.writeFileSync (`${process.env.PWD}/${path}/${filename}`, data, base64 && 'base64' || undefined)

	let file = await models.file.create ({
		path,
		owner,
		filename,
		type,
		label,
		token
	})

	return file
}

/**
 * Upload un fichier sur le serveur, et créé une instance de **file**.
 *
 * @memberof File
 * @method
 * @static
 * @param   {string}  file_      Fichier a upload, en base64.
 * @param   {string}  [owner]    Nom du créateur.
 * @param   {string}  [type=csv] Type.
 * @param   {string}  filename   Nom du fichier, avec l'extension.
 * @param   {boolean} enabled    Indique si le fichier doit etre accessible.
 */
File.upload = async function ({ file_, owner, type, filename, enabled } = { }, { user } = { }) {
	console.function_called (`[${owner || '_'}]`)

	let file = await models.file.generate ({
		base64: file_,
		owner: owner || user,
		filename,
		type,
		enabled,
		path: `uploads/${type}/${filename}`
	})

	return file.download ()
}

/**
 * Action Forest uniquement. Affiche le lien de téléchargement.
 *
 * @memberof File
 * @method
 */
File.prototype.download = function ({ } = { }) {
	console.function_called (`[${this.id || '_'}]`)

	return {
		need_html_view: true,
		link: `<a href = '${hosts.local}/api/v2/files/${this.token}'>Download ${this.filename}</a>`
	}
}

/**
 * Rend le fichier accessible.
 *
 * @memberof File
 * @method
 */
File.prototype.enable = async function ({ } = { }) {
	console.function_called (`[${this.id || '_'}]`)

	await this.update ({
		enabled: true
	})
}

/**
 * Rend le fichier non-accessible.
 *
 * @memberof File
 * @method
 */
File.prototype.disable = async function ({ } = { }) {
	console.function_called (`[${this.id || '_'}]`)

	await this.update ({
		enabled: false
	})
}

/**
 * Renvoie un **file** correspondant au `hash` passé en paramètre.
 *
 * @memberof File
 * @method
 * @param   {string} hash  Hash md5.
 * @returns {File}
 */
File.find_by_hash = async function (hash) {
	return await models.file.findOne ({ where: { hash } })
}

module.exports = File
