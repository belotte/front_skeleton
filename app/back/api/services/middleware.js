const is_token_valid = async function (req, res, next) {
	let token = req.params.token || req.params.body || req.query.token || req.headers['x-access-token'] || (req.headers['authorization'] || '').replace ('Bearer ', '')

	if (!token) {
		return res.status (401).json ({ error: 'missing token' })
	}

	let payload = await jwt.verify (token)

	if (!payload) {
		return res.status (401).json ({ error: 'invalid token' })
	}

	req.payload = payload

	return next ()
}

module.exports = {
	is_token_valid
}
