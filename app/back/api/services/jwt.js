/**
 * Ajoute des fonctionnalités au module `jsonwebtoken`.
 *
 * Par defaut, utilise une signature RS256. Il faut pour cela générer 2 fichiers.
 * Pour ce faire:
 * ```sh
 * cd ~/.ssh
 * ssh-keygen -t rsa -b 4096 -f jwtRS256.key
 * openssl rsa -in jwtRS256.key -pubout -outform PEM -out jwtRS256.key.pub
 * ```
 *
 * @module jwt
 */

const jwt = require ('jsonwebtoken')
const private_key = fs.readFileSync (`${process.env.HOME}/.ssh/jwtRS256.key`).toString ()
const public_key = fs.readFileSync (`${process.env.HOME}/.ssh/jwtRS256.key.pub`).toString ()

/**
 * Créé un JWT.
 *
 * @function create
 * @param   {Object} payload   Contenu du JWT.
 * @param   {string} [secret]  Clé de signature. Si vide, le JWT sera signé avec la clé RS256.
 * @returns {string}   Le JWT généré.
 */
const create = function ({ payload, secret } = { }) {
	const token = jwt.sign (payload, secret || private_key, {
		...(secret && { } || { algorithm: 'RS256' })
	})
	return token
}

/**
 * Vérifie un JWT.
 *
 * @function verify
 * @param   {string} token JWT a vérifier.
 * @param   {string} key   Clé de verification. Si vide, le JWT sera vérifié avec la clé RS256.
 * @returns {Object}    Payload du JWT, si celui-ci est valide. Sinon, `null`.
 */
const verify = function (token, key) {
	return jwt.verify (token, key || public_key, (error, payload) => {
		if (error) { return null }
		return payload
	})
}

const extract_token_from_bearer = ({ bearer }) => {
	if (bearer.split (' ')[0] === 'Bearer') {
		return bearer.split (' ')[1]
	}
	return null
}

/**
 * Renvoie le payload du JWT, sans vérifier la signature.
 * @param   {string} token JWT a vérifier.
 */
const decode = jwt.decode

module.exports = {
	create,
	verify,
	decode,
	extract_token_from_bearer
}
