/**
 * Interface avec l'API de Slack.
 *
 * Une liste de tous les channels est disponible dans le fichier `/config/slack_channels_list`.
 * Il est possible de mettre ce fichier a jour via `tools.update_slack_channels_list ()`.
 *
 * Pour ajouter un nouveau workspace, il faut:
 * - créer une app ici: `https://api.slack.com/apps`;
 * - ajouter un `bot user` (*Bot Users*);
 * - ajouter des scopes a l'app (**OAuth & Permissions**);
 * - installer l'app (**Install App**);
 * - placer le **Bot User OAuth Access Token** dans `slack_channels_list`[workspace].`token`;
 *
 * Si `icon` est un tableau, alors une icon sera choisie au hasard dans ce tableau.
 *
 * Utilisation:
 * await slack.post ({
 * 	...(user && { user: 'frank' } || { channel: 'operations' }),
 * 	username: 'Bot',
 * 	icon: [':see_no_evil:'],
 * 	blocks: [{
 * 		type: 'section',
 * 		text: {
 * 			type: 'mrkdwn',
 * 			text: `Hello World!`
 * 		}
 * 	}]
 * })
 *
 * Documentation: `https://api.slack.com/messaging/managing`.
 *
 * @module slack
 */

const endpoint = 'https://slack.com/api'
const channels_list = require (`${process.env.PWD}/config/slack_channels_list`)
const headers = token => ({
	authorization: `Bearer ${token}`,
	'Content-Type': 'application/json; charset=utf-8'
})

const action_types = {
	BUTTON: 'button'
}

const button_styles = {
	DEFAULT: 'default',
	PRIMARY: 'primary',
	DANGER: 'danger'
}

module.exports = { }
module.exports.conversations = { }
module.exports.chat = { }

module.exports.conversations.list = async ({ workspace = 'workspace_name', cursor } = { }) => {
	let { body } = await requests.get ({
		url: `${endpoint}/conversations.list?token=${channels_list[workspace].token}&exclude_archived=true&limit=500&cursor=${cursor || ''}`
	})

	if (!body.ok) {
		return console.error ({
			code: 801,
			error: body
		})
	}

	return {
		channels: body.channels,
		cursor: body.response_metadata.next_cursor
	}
}

module.exports.conversations.history = async ({ workspace = 'workspace_name', channel_id } = { }) => {
	if (!channel_id) { throw 'channel_id is required.' }

	let { body } = await requests.get ({
		url: `${endpoint}/conversations.history?token=${channels_list[workspace].token}&channel=${channel_id}`
	})

	if (!body.ok) {
		return console.error ({
			code: 801,
			error: body
		})
	}

	return body.messages
}

module.exports.conversations.get_channel_id_from_name = async ({ workspace = 'workspace_name', name } = { }) => {
	if (!channels_list[workspace].channels[name]) {
		return console.error ({
			code: 801,
			error: `Slack channel "${name}" not found`
		})
	}

	return channels_list[workspace].channels[name]
}

module.exports.conversations.get_message = async ({ workspace = 'workspace_name', channel_id, ts } = { }) => {
	let { body } = await requests.get ({
		url: `${endpoint}/conversations.history?token=${channels_list[workspace].token}&channel=${channel_id}&latest=${ts}&limit=1&inclusive=true`
	})

	if (!body.ok) {
		return console.error ({
			code: 801,
			error: body
		})
	}

	return body.messages[0]
}

module.exports.conversations.get_replies = async ({ workspace = 'workspace_name', channel_id, ts } = { }) => {
	let { body } = await requests.get ({
		url: `${endpoint}/conversations.replies?token=${channels_list[workspace].token}&channel=${channel_id}&ts=${ts}`
	})

	return body.messages
}

/**
 * Envoie un message Slack.
 *
 * Si `user` est rempli, alors l'ID correspondant sera récupéré dans `/config/users`.
 * Pour récupérer les IDs pour remplir ce fichier, il faut aller sur le profil Slack de la personne, et cliquer sur les 3 points. Pour l'instant il est a jour.
 *
 * `blocks` utilise la nouvelle feature de Slack qui utilise.. des blocks.
 * La documentation est dispo ici: `https://api.slack.com/block-kit/building#adding_blocks`.
 * Et il est possible de s'entrainer la: `https://api.slack.com/tools/block-kit-builder`.
 *
 * @function post
 * @param   {string}   [workspace=hellozack_family] Workspace défini dans `/config/slack_channels_list`.
 * @param   {string}   channel_id                   ID d'un channel Slack.
 * @param   {string}   channel                      Nom d'un channel, comme définis dans `/config/slack_channels_list`.
 * @param   {string}   user                         Nom d'un user, comme définis dans `/config/users`
 * @param   {string}   text                         Texte a afficher. Inutile si `blocks` est présent.
 * @param   {string[]} icon                         Icone du post. Si tableau, une valeur sera choisie au hasard.
 * @param   {String}   [username=ZackBot]           Nom du posteur.
 * @param   {Object}   blocks                       Blocks. Voir documentation.
 * @returns {Object}   Réponse de l'API Slack. Contient notemment le `ts`.
 */
module.exports.chat.post = async function ({ workspace = 'workspace_name', channel_id, channel, user, text, icon, username = 'ZackBot', blocks } = { }) {
	console.function_called (`[${channel_id || channel || user}]`)

	if (user) {
		channel_id = users[user].slack_id
	}
	if (channel && !channel_id) {
		channel_id = await module.exports.conversations.get_channel_id_from_name ({ workspace, name: channel })
	}

	let { body } = await requests.post ({
		url: `${endpoint}/chat.postMessage`,
		headers: headers (channels_list[workspace].token),
		body: {
			channel: channel_id,
			icon_emoji: Array.isArray (icon) && icon.random_value || icon,
			username,
			as_user: false,
			text,
			mrkdwn: true,
			blocks: (blocks || []).length && [...blocks, {
				type: 'context',
				elements: [{
					type: 'mrkdwn',
					text: `For more info, contact @frank.`
				}]
			}] || []
		}
	})

	if (body && !body.ok) {
		return console.error ({
			code: 801,
			error: body
		})
	}

	return body
}

module.exports.chat.update = async function ({ workspace = 'workspace_name', channel_id, channel, user, text, icon, username = 'ZackBot', ts, blocks } = { }) {
	if (user) {
		channel_id = users[user].slack_id
	}
	if (channel && !channel_id) {
		channel_id = await module.exports.conversations.get_channel_id_from_name ({ workspace, name: channel })
	}

	let { body } = await requests.post ({
		url: `${endpoint}/chat.postMessage`,
		headers: headers (channels_list[workspace].token),
		body: {
			channel: channel_id,
			icon_emoji: Array.isArray (icon) && icon.random_value || icon,
			username,
			as_user: false,
			text,
			ts,
			mrkdwn: true,
			blocks
		}
	})

	if (!body.ok) {
		return console.error ({
			code: 801,
			error: body
		})
	}

	return body
}

module.exports.chat.delete = async ({ workspace = 'workspace_name', channel_id, channel, ts } = { }) => {
	if (channel && !channel_id) {
		channel_id = await module.exports.conversations.get_channel_id_from_name ({ workspace, name: channel })
	}

	let { body } = await requests.post ({
		url: `${endpoint}/chat.delete`,
		headers: headers (channels_list[workspace].token),
		body: {
			channel: channel_id,
			ts
		}
	})

	if (!body.ok) {
		return console.error ({
			code: 801,
			error: body
		})
	}

	return true
}

module.exports.chat.respond = async ({ workspace = 'workspace_name', channel_id, user, username, icon, ts, text, broadcast, as_user = false } = { }) => {
	if (user) {
		channel_id = users[user].slack_id
	}
	let color = null
	let { body } = await requests.post ({
		url: `${endpoint}/chat.postMessage`,
		headers: headers (channels_list[workspace].token),
		body: {
			channel: channel_id,
			text,
			thread_ts: ts,
			username,
			icon_emoji: Array.isArray (icon) && icon.random_value || icon,
			reply_broadcast: !!broadcast,
			mrkdwn: true
		}
	})

	if (!body.ok) {
		return console.error ({
			code: 801,
			error: body
		})
	}

	return body.message
}

module.exports.reactions = { }

module.exports.reactions.add = async function ({ workspace = 'workspace_name', channel_id, channel, user, emoji, ts } = { }) {
	if (user) {
		channel_id = users[user].slack_id
	}
	if (channel && !channel_id) {
		channel_id = await module.exports.conversations.get_channel_id_from_name ({ workspace, name: channel })
	}

	let { body } = await requests.post ({
		url: `${endpoint}/reactions.add`,
		headers: headers (channels_list[workspace].token),
		body: {
			channel: channel_id,
			name: emoji,
			timestamp: ts,
			ts
		}
	})

	if (!body.ok) {
		return console.error ({
			code: 801,
			error: body
		})
	}

	return body
}

module.exports.post = module.exports.chat.post
