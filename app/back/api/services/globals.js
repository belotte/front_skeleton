require ('colors')
global.fs = require ('fs')
global.ms = require ('ms')
global.moment = require ('moment')

global.moment.locale ('fr')
global.moment.prototype.beautify = function () {
	return this.format ('dddd DD MMMM, HH:mm:ss')
}

global.jwt = require (`${process.env.PWD}/api/services/jwt`)
global.slack = require (`${process.env.PWD}/api/services/slack`)
global.tools = require (`${process.env.PWD}/api/services/tools`)
global.hosts = require (`${process.env.PWD}/config/hosts`)

const { make_request } = require ('async_requests')

global.requests = {
	post: make_request ('post'),
	get: make_request ('get'),
	put: make_request ('put'),
	delete: make_request ('delete'),
	custom: make_request ()
}

const characters = {
	numeric: [...'0123456789'],
	alphanumeric: [...'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'],
	alpha: [...'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'],
}

global.wrap = fn => (...args) => fn(...args).catch(args[2])
global.transaction_wrap = fn => {
	if (namespace.get ('transaction')) {
		return fn (namespace.get ('transaction'))
	}
	return sequelize.transaction ({ isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED }, fn)
}
global.now = date => moment (date).format ('YYYY-MM-DD HH:mm:ss')
global.now_minimal = date => moment (date).format ('YYMMDD_HHmmss_SSS')
global.date_from_minimal = date => moment (date, 'YYMMDD_HHmmss_SSS')
global.rand = (length, type = 'alphanumeric', modifier = 'toString') => [...Array (length)].map (i => characters[type][Math.random () * characters[type].length | 0]).join ('')[modifier] ()
global.require_and_reload = path => {
	try {
		delete require.cache[require.resolve (path)]
	} catch (error) { }
	return require (path)
}
global.wait = async (delay) => new Promise (resolve => { setTimeout (() => resolve (), delay * 1000) })

global.empty_instance = {
	update: () => { }
}

try {
	Object.keys (require (`${process.env.PWD}/config/globals`)).map (key => {
		global[key] = require (`${process.env.PWD}/config/globals`)[key]
	})
} catch (error) { }

Object.defineProperty (Array.prototype, 'random_value', {
	get: function () {
		return this[Math.floor (Math.random () * this.length)]
	}
})
Object.defineProperty (Array.prototype, 'last_element', {
	get: function () {
		return this[this.length - 1]
	}
})
Object.defineProperty (Array.prototype, 'push_', {
	value: function (value_to_push) {
		this.push (value_to_push)
		return this
	}
})

Object.defineProperty (Object.prototype, 'is_empty', {
	get: function () {
		for (let key in this) {
			if (this.hasOwnProperty (key))
			return false
		}
		return true
	}
})
Object.defineProperty (Object.prototype, 'at_path', {
	value: function (path) {
		return path.split ('.').reduce ((final_object, key) => { return final_object && final_object[key] || undefined }, this)
	}
})
Object.defineProperty (Object.prototype, 'is_equal_to', {
	value: function (object) {
		if (Object.keys (this).sort ().join () !== Object.keys (object).sort ().join ()) {
			return false
		}
		for (let key of Object.keys (this)) {
			if (this[key] !== object[key]) {
				return false
			}
		}
		return true
	}
})
Object.defineProperty (Error.prototype, 'to_string', {
	value: function () {
		let stack_components = this.stack.split ('at ')[1].replace (/\n/g, '')
		stack_components = {
			filename: stack_components.match (/\(?(.*\.js)/)[1].replace (process.env.PWD, ''),
			function: stack_components.split (' ')[0].includes (process.env.PWD) && '-' || stack_components.split (' ')[0],
			position: stack_components.match (/:([0-9]*:[0-9]*)/)[1]
		}

		return {
			name: this.name,
			message: this.message,
			file: stack_components.filename,
			function: stack_components.function,
			position: stack_components.position
		}
	}
})
Object.defineProperty (Object.prototype, 'extract', {
	value: function (keys) {
		let new_object = { }
		for (let key of keys) {
			if (!Array.isArray (key)) {
				key = [key, key]
			}
			new_object[key[1]] = this[key[0]]
		}
		return new_object
	}
})
Object.defineProperty (Object.prototype, 'extract_all_but', {
	value: function (keys) {
		let new_object = {...this}
		for (let key of keys) {
			delete new_object[key]
		}
		return new_object
	}
})
Object.defineProperty (Object.prototype, 'to_query_params', {
	get: function () {
		return Object.entries (this).filter (([_, value]) => `${value}`.length).map (([key, value]) => `${key}=${value}`).join ('&')
	}
})

Object.defineProperty (Number.prototype, 'round', {
	value: function (number_of_decimal = 0) {
		return +(`${Math.round (this + `e+${number_of_decimal}`)}e-${number_of_decimal}`)
	}
})

Object.defineProperty (String.prototype, 'decamelized', {
	get: function () {
		return this.replace (/([A-Za-z])([A-Z])/g, '$1_$2').toLowerCase ()
	}
})
Object.defineProperty (String.prototype, 'first_letter_uppercased', {
	get: function () {
		return this.replace(/^\w/, character => character.toUpperCase ())
	}
})
Object.defineProperty (String.prototype, 'hide_password', {
	value: function () {
		return this.replace (/("?password"?: ").*(")/g, '$1********$2')
	}
})
Object.defineProperty (String.prototype, 'modulo', {
	value: function (divisor) {
		return Array.from (this).map (c => parseInt (c)).reduce ((remainder, value) => (remainder * 10 + value) % divisor, 0)
	}
})
Object.defineProperty (String.prototype, 'capitalize', {
	get: function () {
		return this.toLowerCase ().replace (/(^|\s)([a-z])/g, (_, previous, character_to_upper) => previous + character_to_upper.toUpperCase ())
	}
})
Object.defineProperty (Object.prototype, 'too_long_strings_removed', {
	get: function () {
		return Object.keys (this).reduce ((result, key) => {
			let new_value = null
			if (typeof this[key] === 'string' && this[key].length > 2000) {
				new_value = 'Too long to be stored.'
			}
			else if (Array.isArray (this[key])) {
				new_value = this[key].too_long_strings_removed
			}
			else if (this[key] && typeof this[key] === 'object') {
				new_value = this[key].too_long_strings_removed
			}
			else {
				new_value = this[key]
			}
			if (Array.isArray (this)) {
				if (!Array.isArray (result)) {
					result = []
				}
				result.push (new_value)
			}
			else {
				result[key] = new_value
			}
			return result
		}, { })
	}
})


let loggers = {
	info: console.info,
	error: console.error,
	function_called: console.info
}

for (let type in loggers) {
	console[type] = function () {
		args = Object.keys (arguments).map (id => arguments[id]).map (arg => {
			if (typeof arg === 'object') {
				return JSON.parse (`${JSON.stringify (arg, null, 2).hide_password ()}`)
			}
			return (arg || '').toString ().replace (/^"|"$/g, '')
		})
		if (type === 'error' && process.env.NODE_ENV === 'production') {
			let args_for_slack = Object.keys (arguments).map (id => arguments[id]).map (arg => {
				if (typeof arg === 'object') {
					return `\`\`\`${JSON.stringify (arg, null, 2)}\`\`\``
				}
				return (arg || '').toString ().replace (/^"|"$/g, '')
			})
			let text = ''
			for (let arg of args_for_slack) {
				text = `${text && `${text}\n\n`}> ${arg}`
			}
		}
		let stack_component = new Error ().stack.split ('at ')[2].replace (/\n/g, '')
		let caller_components = {
			filename: stack_component.match (/\/([0-9a-zA-Z_]*\.js)/)[1].replace (process.env.PWD, ''),
			function: stack_component.split (' ')[0].includes (process.env.PWD) && '-' || stack_component.split (' ')[0],
			line: stack_component.match (/:([0-9]*:[0-9]*)/)[1]
		}

		loggers[type].apply (console, [`[${now_minimal ()}]${['error', 'function_called'].includes (type) && ` [${caller_components.filename}:${caller_components.line.split (':')[0]}][${caller_components.function.replace (/^Object\./, '')}]` || ''}`[{
			info: 'gray',
			error: 'red',
			function_called: 'blue'
		}[type]], ...args])

		if (['error'].includes (type) && arguments[0] && typeof arguments[0] === 'object') {
			arguments[0].logged = true
		}

		return Object.keys (arguments).map (id => arguments[id])[0]
	}
}

if (process.env.DO_NOT_MODIFY_LOGGERS) {
	console.info = loggers.info
	console.error = loggers.error
	if (!process.env.LOG_FUNCTIONS) {
		console.function_called = () => { }
	}
}
