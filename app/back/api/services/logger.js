/**
 * Chaque instance permet de sauvegarder des logs dans des fichiers.
 *
 * Les fichiers de log sont forcement des JSON. Si une extension est définie, elle sera remplacée.
 * `path` est un chemin absolu qui part de `${process.env.PWD}/logs`.
 *
 * Il y a 2 types de loggers:
 * - logger: log toujours dans le même fichier;
 * - pusher: prend en paramètre un nom de fichier dans
 *
 * Utilisation:
 * ```
 * const Logger = require (`${process.env.PWD}/api/services/logger`)
 * const loggers = {
 *	 logger: Logger ({
 *		 path: 'logs/log.json'
 *	 }),
 *	 pusher: Logger ({
 *		 path: 'logs/log.json',
 *		 type: 'pusher'
 *	 })
 * }
 * logger.logger (42) == logger.logger.log (42)
 * loggers.pusher.push ({
 *	 element: { },
 *	 filename: ''
 * })
 * loggers.pusher ({
 *	 element: { },
 *	 filename: ''
 * })
 * ```
 *
 * @class Logger
 */

const extension = /(\.[a-z]*)?$/
const double_slash = /\/\//g
const last_path_element = /(.*)\/[^/]*/

const Logger = function ({ path, is_request, type = 'logger' }) {
	if (!path) {
		throw console.error ({
			error: '`path` must be defined'
		})
	}

	if (['logger'].includes (type)) {
		path = `${process.env.PWD}/logs/${(path || '').replace (extension, '.json')}`.replace (double_slash, '/')

		try {
			fs.mkdirSync (path.match (last_path_element)[1], 0o755)
		} catch (error) { if (!['EEXIST'].includes (error.code)) { throw error } }
	}
	if (['pusher'].includes (type)) {
		path = `${process.env.PWD}/logs/${(path || '').replace (extension, '')}`.replace (double_slash, '/')

		try {
			fs.mkdirSync (path, 0o755)
		} catch (error) { if (!['EEXIST'].includes (error.code)) { throw error } }
	}

	let logger_ = logger (path, is_request)
	let pusher_ = pusher (path)

	Object.defineProperty (logger_, 'log', {
		value: function (parameters) {
			this (parameters)
		}
	})
	Object.defineProperty (logger_, 'push', {
		value: function (parameters) {
			this (parameters)
		}
	})
	Object.defineProperty (pusher_, 'push', {
		value: function (parameters) {
			this (parameters)
		}
	})

	switch (type) {
		case 'logger': return logger_
		case 'pusher': return pusher_
	}
}

/**
 * Formatte (si `is_request` est `vrai`) et sauvegarde `message` dans `path`.
 *
 * @memberof Logger
 * @method
 * @param   {Object} message Objet, string, number ou autre valeur a sauvegarder.
 */
const logger = (path, is_request) => function (message) {
	if (typeof message !== 'object') {
		throw 'Logger only works with objects'
	}

	if (is_request) {
		const { user_id, identifier } = jwt.decode ((message.params || { }).token || (message.body || { }).token || (message.query || { }).token || (message.headers || { })['x-access-token'] || ((message.headers || { })['authorization'] || '').replace ('Bearer ', '')) || { }
		message = {
			body: message.body && message.body.attributes && {
				ids: message.body.data && message.body.data.attributes && message.body.data.attributes.ids,
				values: message.body && message.body.data && message.body.data.attributes && message.body.data.attributes.values,
				collection: message.body && message.body.data && message.body.data.attributes && message.body.data.attributes.collection_name
			} || message.body || { },
			pretty: `[ ${moment ().format (`YYMMDD_HHmmss_SSS`)} ] [ ${message.ip || message._remoteAddress || (message.connection && message.connection.remoteAddress) || ''} ] [ ${message.user && message.data.email.split ('@')[0] || (message.user || { }).name || (message.user || { }).id || message.forest_user || user_id || (identifier && `#${identifier}` || null) || '-'} ] -> [ ${(message.method || '').toUpperCase ()} ] ${message.originalUrl}`
		}
	}

	message.timestamp = now_minimal ()

	let array = []
	try {
		array = require_and_reload (path)
	} catch (error) { }

	array.push (JSON.parse (JSON.stringify (message.too_long_strings_removed).hide_password ()))
	fs.writeFileSync (`${path}.json`, JSON.stringify (array, null, '\t'), 'utf-8')
}

/**
 * Permet de logger dans des fichiers séparés (par exemple dont le nom est un SKU), pour isoler plus facilement les logs.
 *
 * @memberof Logger
 * @method
 * @param   {string} filename  Nom du fichier.
 * @param   {Object} element   Element a logger.
 */
const pusher = (path) => function ({ filename, element }) {
	let current_file_path = path

	if (filename) {
		current_file_path = `${current_file_path}/${filename.replace (extension, '.json')}`
	}

	let array = []
	try {
		array = require_and_reload (current_file_path)
	} catch (error) { }

	array.push (JSON.parse (JSON.stringify (element.too_long_strings_removed).hide_password ()))
	fs.writeFileSync (current_file_path, JSON.stringify (array, null, '\t'), 'utf-8')
}

module.exports = Logger
