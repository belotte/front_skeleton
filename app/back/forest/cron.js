const Liana =	require ('forest-express-sequelize')
const { actions_modifier } = require (`${process.env.PWD}/config/forest`)

Liana.collection ('cron', {
	actions: [{
		name: 'add_remark',
		fields: [{
			field: 'remark',
			type: 'String'
		}]
	}, {
		name: 'close',
		fields: []
	}, {
		name: 'retry',
		fields: []
	}].map (actions_modifier),

	fields: []
})
