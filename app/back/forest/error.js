const Liana =	require ('forest-express-sequelize')
const { actions_modifier } = require (`${process.env.PWD}/config/forest`)

Liana.collection ('error', {
	actions: [{
		name: 'add_remark',
		fields: [{
			field: 'remark',
			type: 'String'
		}]
	}, {
		name: 'retry',
		fields: []
	}].map (actions_modifier),

	fields: []
})
