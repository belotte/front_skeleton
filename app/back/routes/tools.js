/**
 * @namespace Routes.Ping
 */

const router =	require ('express').Router ()

router.route ('/functions/:name/:type?')
.all (wrap (async (req, res) => {
	let f = require_and_reload (`${process.env.PWD}/api/services/tools_functions`)[req.params.name]

	if (!f || typeof f !== 'function') {
		return res.status (404).json ({ error: `${req.params.name} is not a function` })
	}

	console.info (`[function][${req.params.name}] running.`)

	let result = await f (req.method === 'POST' && req.body || req.query)

	console.info (`[function][${req.params.name}] done.`)

	return res[{
		json: 'json',
		text: 'end'
	}[req.params.type || 'json']] (result)
}))

router.route ('/update_slack_channels_list')
.get (async (req, res) => {
	for (let workspace of Object.keys (require (`${process.env.PWD}/config/slack_channels_list.json`))) {
		let channels = []
		let cursor = ''

		do {
			;({ channels: new_channels, cursor } = await slack.conversations.list ({ workspace, cursor }))
			channels = [...channels, ...new_channels]
			channels = channels.map (channel => channel.extract (['id', 'name']))

			let old_file = require_and_reload (`${process.env.PWD}/config/slack_channels_list.json`)
			let data = old_file[workspace].channels

			for (let { id, name } of channels) {
				if (/^c-/.test (name)) {
					continue
				}
				data[name] = id
			}

			fs.writeFileSync (`${process.env.PWD}/config/slack_channels_list.json`, JSON.stringify ({
				...old_file,
				[workspace]: {
					...old_file[workspace],
					cursor,
					channels: data
				}
			}, null, '\t'))

			if (!cursor) {
				break
			}

			await wait (2)
		} while (channels.length)
	}

	return res.end ()
})

module.exports = router
