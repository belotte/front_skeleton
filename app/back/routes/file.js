/**
 * @namespace Routes.File
 */

const router =	require ('express').Router ()

router.route ('/:token')
.get (wrap (async (req, res) => {
	let file = await models.file.findOne ({
		where: {
			token: req.params.token
		}
	})

	if (!file) {
		return res.status (404).end ()
	}

	if (!file.enabled) {
		return res.status (403).end ()
	}

	if (!fs.existsSync (`${process.env.PWD}/${file.path}/${file.filename}`)) {
		return res.status (404).end ()
	}

	await file.update ({
		number_of_downloads: (file.number_of_downloads | 0) + 1
	})

	return res.download (`${process.env.PWD}/${file.path}/${file.filename}`, file.filename)
}))

module.exports = router
