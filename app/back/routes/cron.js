/**
 * @namespace Routes.Cron
 */

const router =	require ('express').Router ()
const crons =	require (`${process.env.PWD}/api/services/crons`)

/**
 * Run action defined in `/api/services/cron`.
 *
 * A Cron instance is created, and set as `pending`.
 * If a `cron_id` is given, the corresponding **cron** is set to `retried` and the new **cron** is assigned to it.
 *
 * If an error appends, the **cron** is set to `failed`, and the error is saved to **cron**.`error`.
 *
 * Else, the **cron** is set to `done` and it's `execution_time` is saved.
 *
 * @mermaid
 * 	graph LR;
 * 		START((request)) --> A[create new cron]
 * 		A --> B{params.cron_id ?}
 * 		B --> |YES| C[new cron is assigned to old one]
 * 		B --> |NO| D[_]
 * 		C --> D
 * 		D --> END((response))
 * 		D --> E{action is performed}
 * 		E --> F{success ?}
 * 		F --> |YES| G[cron.status = done]
 * 		F --> |NO| H[cron.status = failed]
 *
 * @name Run Cron
 * @memberOf Routes.Cron
 * @function
 * @path {GET} /api/v2/runs/:action/:cron_id?
 * @params {String} :action Name of the action to run
 * @params {Number} [:cron_id] ID of the retried cron
 */
router.route ('/run/:action/:cron_id?')
.get (wrap (async (req, res, next) => {
	try {
		let begin = Date.now ()
		let cron = await models.cron.create ({
			action: req.params.action,
			status: 'pending'
		})

		if (req.params.cron_id) {
			let old_cron = await models.cron.findOne ({
				where: {
					id: req.params.cron_id
				}
			})

			if (old_cron) {
				await old_cron.update ({
					status: 'retried',
					next_try_id: cron.id
				})
			}
		}

		if (!crons[req.params.action]) {
			await cron.update ({
				status: 'not found'
			})
			return res.status (404).json ({
				error: 'This action does not exist'
			})
		}

		if (!req.query.wait) {
			res.end ()
		}

		try {
			let begin = Date.now ()
			await crons[req.params.action] ()
			await cron.update ({
				status: 'done',
				success: true,
				execution_time: (Date.now () - begin) / 1000
			})
		} catch (error) {
			await cron.update ({
				error: JSON.stringify (error, null, 2),
				success: false,
				status: 'failed'
			})
		}

		if (req.query.wait) {
			return res.json ({
				duration: Date.now () - begin
			})
		}
	} catch (error) {
		console.error (error)
		return res.status (500).json ({
			error: 'An error occured. Please contact admin.'
		})
	}
}))

module.exports = router
