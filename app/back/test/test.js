require (`${process.env.PWD}/index`)
global.chai =	require ('chai')
global.should =	chai.should ()
global.assert =	chai.assert
global.expect =	chai.expect
let server_ready = false

process.on ('server_started', () => { server_ready = true })

describe ('Tests', function () {
	before (function (done) {
		setTimeout (done, !server_ready * 50 * 1000)
		process.on ('server_started', done)
	})

	this.timeout (60 * 1000)

	let files_to_test = [
		['<test_name>', '<test_file>', !!'enabled']
	]

	require ('colors').setTheme ({
		bold_blue: ['blue', 'bold'],
		dim_gray: ['dim', 'gray'],
	})
	console.info (`\nenabled test files: ${files_to_test.map (file => file[1][file[2] && 'bold_blue' || 'dim_gray']).join (', ')}`)

	for (let file of files_to_test) {
		if (!file[2]) { continue }
		describe (file[0], function () {
			require (`./${file[1]}`)
		})
	}

	after (() => {
		setTimeout (() => {
			process.exit ()
		}, 500)
	})
})
